<?php

namespace app\controllers;

use app\models\Maillot;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MaillotsController implements the CRUD actions for Maillot model.
 */
class MaillotsController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Maillot models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Maillot::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'código' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Maillot model.
     * @param string $código Código
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($código)
    {
        return $this->render('view', [
            'model' => $this->findModel($código),
        ]);
    }

    /**
     * Creates a new Maillot model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Maillot();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'código' => $model->código]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Maillot model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $código Código
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($código)
    {
        $model = $this->findModel($código);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'código' => $model->código]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Maillot model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $código Código
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($código)
    {
        $this->findModel($código)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Maillot model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $código Código
     * @return Maillot the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($código)
    {
        if (($model = Maillot::findOne(['código' => $código])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
